::
:: License: GPLv3
::

@ECHO OFF
::path constants
SET TEMP_DIR=D:\temp\tlp-out\
SET HTML_OUT_DIR=%TEMP_DIR%tlp-side-by-side\
SET HTML_HYPER_OUT_DIR=%TEMP_DIR%tlp-side-by-side-hyperlinked\

::clean up
ECHO cleaning up
del /q %TEMP_DIR%
FOR /D %%p IN (%TEMP_DIR%\*) DO rmdir "%%p" /s /q

::create directory structure
ECHO creating output directories
md %HTML_OUT_DIR%
md %HTML_HYPER_OUT_DIR%

ECHO creating temporary tex files
php create_tlp_latex_version.php > %TEMP_DIR%"tlp.tex"
php json_tex2html.php > %TEMP_DIR%"tlp_html.json"

ECHO creating html version
php create_tlp_html_version.php > %HTML_OUT_DIR%tlp.html
ECHO creating hyperlinked html version
php create_tlp_html_version.php nocolumns > %HTML_HYPER_OUT_DIR%tlp-hyperlinked.html

::copy resources
ECHO copying resources 
xcopy .\images\*  %HTML_OUT_DIR%\images /s/i/q >nul
xcopy .\images\*  %HTML_HYPER_OUT_DIR%\images /s/i/q >nul

copy cover.jpg  %HTML_OUT_DIR% >nul
copy cover.jpg  %HTML_HYPER_OUT_DIR% >nul

:: remove unused files
ECHO deleting temporary files
del /q %TEMP_DIR%

ECHO Done!