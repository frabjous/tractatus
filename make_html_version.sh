#!/bin/bash
#
# License: GPLv3
#

#path constants
TEMP_DIR="$HOME/tlp-out"
HTML_OUT_DIR="$TEMP_DIR/tlp-side-by-side"
HTML_HYPER_OUT_DIR="$TEMP_DIR/tlp-side-by-side-hyperlinked"

echo "Cleaning up!"
rm -rf "$TEMP_DIR"

mkdir -p "$TEMP_DIR"
mkdir -p "$HTML_OUT_DIR"
mkdir -p "$HTML_HYPER_OUT_DIR"

echo "Creating temporary tex files"
php create_tlp_latex_version.php > "$TEMP_DIR/tlp.tex"
php json_tex2html.php > "$TEMP_DIR/tlp_html.json"

echo "Creating the html files"
php create_tlp_html_version.php > "$HTML_OUT_DIR/tlp.html"
php create_tlp_html_version.php nocolumns > "$HTML_HYPER_OUT_DIR/tlp-hyperlinked.html"

echo "Copying resources"
cp cover.jpg "$HTML_OUT_DIR"
cp cover.jpg "$HTML_HYPER_OUT_DIR"
cp -r images "$HTML_OUT_DIR"
cp -r images "$HTML_HYPER_OUT_DIR"

echo "Removing temporary files"
rm "$TEMP_DIR/tlp.tex"
rm "$TEMP_DIR/tlp_html.json"

echo "Done!"



