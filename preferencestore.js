// License: GPLv3

/**
 * a thin wrapper:
 * use localstorage as preference store.
 * store string / int / float / boolean values as key value pairs
 * checks whether local storage is available when constructed
 * if not available:
 *  calls to putValue are ignored with an entry in console log
 *  calls to getValue return the given default value, no log message
 * 
 * the type is stored alongside the value. as delimiter :: is used
 * 
 * to check availability use the isAvailable() method.    
 */

function PreferenceStore() {

    this.available = isLocalStorageAvailable();
    this.prefix = '';
    var typeValueDelimiter = '::';

    this.setPrefPrefix = function(prefix){
        this.prefix = prefix;
    }
    
    this.putValue = function (key, value, dataType) {
        //no local storage available? --> go away 
        if(!this.available){
            console.error('call to putValue() of PreferenceStore when not available!\n check with isAvailable() before using the method.');
            return;
        }

        if (dataType === undefined) dataType = 'string';
        var stored = value + typeValueDelimiter + dataType;
        localStorage.setItem(this.prefix + key, stored);
    }

    this.getValue = function (key, defaultValue) {
        //no local storage available? --> return default 
        if(!this.available){
            if(defaultValue === undefined)return null;
            return defaultValue;
        }

        var val = localStorage.getItem(this.prefix + key);
        if (val !== null) {
            return parseValue(val);
        }
        if (defaultValue === undefined) return null;
        return defaultValue;
    }
    this.clear = function () {
        var toBeRemoved = [];
        var i;
        for (i = 0; i < localStorage.length; i++){
            var found = localStorage.key(i);
            if(found.startsWith(this.prefix)) toBeRemoved.push(found);
        }
        for (i = 0; i < toBeRemoved.length; i++){
            localStorage.removeItem(toBeRemoved[i]);
        }
    }
    this.isAvailable = function(){
        return this.available;
    }

    function parseValue(val) {
        var pair = val.split(typeValueDelimiter);
        if (pair[1] === 'string') return pair[0];
        if (pair[1] === 'int') return parseInt(pair[0]);
        if (pair[1] === 'float') return parseFloat(pair[0]);
        if (pair[1] === 'boolean') return pair[0] === 'true' ? true : false;
        return eval(pair[0]);
    }
    function isLocalStorageAvailable(){
        var key = 'org.XXXX____YYYY.test';
        var value = 'delete_me';
        
        try{
            if(!window.localStorage) return false;
            localStorage.setItem(key, value);
            var result = localStorage.getItem(key);
            localStorage.removeItem('key');
            if(result === value) return true;
         }catch(e){
            return false;
         }
        return false; 
     }
}