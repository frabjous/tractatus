#!/usr/bin/env bash
#
# License: GPLv3
#
cleanmode="yes"
if [[ "$1" == "--noclean" ]] ; then
    cleanmode="no"
fi

source "$HOME/bin/kckcolor.sh"

tempdir="$(mktemp -d)"

bldred='\e[1;31m' # Red
bldylw='\e[1;33m' # Yellow
txtrst='\e[0m'    # Text Reset

echored () {
    echo -e ${bldred}"$@"${txtrst}
}

echoyellow () {
    echo -e ${bldylw}"$@"${txtrst}
}

exit_nicely() {
    cd "$HOME"
    if [[ "$cleanmode" != "no" ]] ; then
        rm -rf "$tempdir"
    fi
    echored "$1" >&2
    exitwitherror 1
}

# find the folder I should be in
scriptloc="$(readlink -f "$0")"
scriptdir="$(dirname "$scriptloc")"
cd "$scriptdir"

outputdir="$scriptdir/output"

if [[ ! -d "$outputdir" ]] ; then
    echoyellow "Creating output folder."
    mkdir -p "$outputdir"
fi

echoyellow "Creating LaTeX versions source code."
php create_tlp_latex_version.php > "$tempdir/tlp.tex"
php create_tlp_latex_version.php ebook_latex_settings.json > "$tempdir/tlp-ebook.tex"
php create_tlp_latex_version.php hierarchy_version_settings.json > "$tempdir/tlp-hierarchy.tex"

echoyellow "Converting LaTeX-based JSON to HTML based JSON."
php json_tex2html.php > "$tempdir/tlp_html.json"

# check if new text is available
if ! diff -q "tlp_html.json" "$tempdir/tlp_html.json" ; then
    echored "replacing tlp_html.json with $tempdir/tlp_html.json";
    cp "$tempdir/tlp_html.json" "tlp_html.json"
fi

echoyellow "Creating HTML sources."
php create_tlp_html_version.php > "$tempdir/tlp.html"
php create_tlp_html_version.php nocolumns > "$tempdir/tlp-hyperlinked.html"
php create_tlp_html_version.php "nocolumns,epub" > "$tempdir/tlp-epubsource.html"

echoyellow "Copying resources to temporary folder."
cp cover.jpg "$tempdir"
cp -r images "$tempdir"
cd "$tempdir"

echoyellow "Running pdfLaTeX..."
echo "Compiling columned LaTeX to PDF."
pdflatex "tlp.tex" > /dev/null || exit_nicely "Compilation failed."
echo "Re-compiling columned LaTeX to PDF."
pdflatex "tlp.tex" > /dev/null || exit_nicely "Compilation failed."

echo "Compiling small page LaTeX to PDF."
pdflatex "tlp-ebook.tex" > /dev/null || exit_nicely "Compilation failed."
echo "Re-compiling small page LaTeX to PDF."
pdflatex "tlp-ebook.tex" > /dev/null || exit_nicely "Compilation failed."

echo "Compiling hierarchy LaTeX to PDF."
pdflatex "tlp-hierarchy.tex" > /dev/null || exit_nicely "Compilation failed."
echo "Re-compiling hierarchy LaTeX to PDF."
pdflatex "tlp-hierarchy.tex" > /dev/null || exit_nicely "Compilation failed."

echoyellow "Creating ePub with calibre. (This may take awhile.)"
ebook-convert "tlp-epubsource.html" "tlp.epub" --cover cover.jpg --disable-remove-fake-margins || exit_nicely "Conversion failed."

echoyellow "Copying to output folder."
cp tlp.pdf tlp-ebook.pdf tlp-hierarchy.pdf tlp.epub tlp.html tlp-hyperlinked.html "$outputdir" || exit_nicely "Copying failed."

cd "$HOME"
rm -rf "$tempdir"
exit 0
